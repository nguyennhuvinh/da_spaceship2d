using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentFly : MyBehaviour
{
    [SerializeField] protected float moveSpeed = 7f;
    [SerializeField] protected Vector3 direction;

    void Update()
    {
        transform.parent.Translate(this.direction * this.moveSpeed * Time.deltaTime);
    }
}
