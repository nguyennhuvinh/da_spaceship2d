using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
public class Loading : MonoBehaviour
{
    public Image LoadingFill_UI;
    public float TimeDuration;
    public TMP_Text loading_text;



    private void Start()
    {
        LoadingFill();
    }

    void LoadingFill()
    {
        // Tween the fill amount of the loading bar and update the text
        DOTween.To(() => 0, value =>
        {
            LoadingFill_UI.fillAmount = value / 100f;
            loading_text.text = value.ToString("F0") + "%";
        }, 100, TimeDuration).OnComplete(() =>
        {
            SceneManager.LoadScene("Game");
        });
    }
}
