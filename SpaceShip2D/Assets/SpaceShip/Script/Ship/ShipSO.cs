using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="ShipSO", menuName = "ScriptableObject/Ship")]
public class ShipSO : ScriptableObject
{
    public string shipName = "Ship";
    public int hpMax = 2;
}
