using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pattern;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameUI : Singleton<GameUI>
{
    [Header("---------Lose_UI--------")]
    [SerializeField] GameObject Panel_Lose_UI;
    [SerializeField] CanvasGroup DarkBG_Lose;

    public IEnumerator LoseUI_ON()
    {
        yield return new WaitForSeconds(1f);
        Panel_Lose_UI.SetActive(true);
        DarkBG_Lose.DOFade(1, 0.3f);
        Time.timeScale = 0;
    }


    public void RestartGame()
    {
        SceneManager.LoadScene("Loading");
        Time.timeScale = 1;
    }

    public void BackMenuGame()
    {
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;
    }
}
