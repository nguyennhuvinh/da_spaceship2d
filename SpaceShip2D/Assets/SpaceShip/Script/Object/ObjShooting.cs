using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjShooting : MyBehaviour
{
    [SerializeField] protected bool isShooting = false;
    [SerializeField] protected float shootDelay = 0.2f;
    [SerializeField] protected float shootTimer = 0f;

    [SerializeField] protected Transform[] firePoints;  // C�c v? tr� b?n �?n
    [SerializeField] protected Vector3[] directions;

    void Update()
    {
        this.IsShooting();
    }

    private void FixedUpdate()
    {
        this.Shooting();
    }

    protected virtual void Shooting()
    {
        this.shootTimer += Time.fixedDeltaTime;

        if (!this.isShooting) return;
        if (this.shootTimer < this.shootDelay) return;
        this.shootTimer = 0;

        for (int i = 0; i < firePoints.Length; i++)
        {
            Vector3 spawnPos = firePoints[i].position;
            Quaternion rotation = transform.parent.rotation;

            Transform newBullet = BulletSpawner.Instance.Spawn(BulletSpawner.bulletOne, spawnPos, rotation);
            AudioManager.Instance.PlaySFX(AudioManager.Instance.Shooting);
            if (newBullet == null) return;

            newBullet.gameObject.SetActive(true);
            BulletCtrl bulletCtrl = newBullet.GetComponent<BulletCtrl>();
            bulletCtrl.SetShotter(transform.parent);
        }
    }

    protected abstract bool IsShooting();
    
        
    
}
