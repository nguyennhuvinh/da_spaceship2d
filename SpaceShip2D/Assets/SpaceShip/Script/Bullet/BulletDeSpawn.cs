using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDeSpawn : DespawnByDistance
{
    public override void DespawnObject()
    {
        BulletSpawner.Instance.Despawn(transform.parent);
    }
}
