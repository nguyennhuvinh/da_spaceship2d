using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pattern;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuUI : Singleton<MenuUI>
{
    [Header("--------Shared--------")]
    [SerializeField] GameObject panelUI_right;
    [SerializeField] GameObject panelUI_Left;

    [Header("--------AudioSettings------")]
    [SerializeField] GameObject Panel_Setting;
    [SerializeField] public GameObject uiMuteAudio;
    [SerializeField] public GameObject uiMuteSFX;


    private void Start()
    {
        UIShared_On();
        CheckSFXMuteState();
        CheckMusicMuteState();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Loading");
    }

   

    private void UIShared_On()
    {
        panelUI_Left.SetActive(true);
        panelUI_right.SetActive(true);
    }

    public void SettingUI()
    {

        Panel_Setting.gameObject.SetActive(!Panel_Setting.activeSelf);
    }

    private void CheckMusicMuteState()
    {
        if (PlayerPrefs.HasKey("MuteMusic"))
        {
            bool isMuted = PlayerPrefs.GetInt("MuteMusic") == 1;
            uiMuteAudio.SetActive(isMuted);
            if (isMuted)
            {
                AudioManager.Instance.musicBackgroundOff();
            }
            else
            {
                AudioManager.Instance.musicBackgroundOn();
            }
        }
    }
    private void CheckSFXMuteState()
    {
        if (PlayerPrefs.HasKey("MuteSFX"))
        {
            bool isMuted = PlayerPrefs.GetInt("MuteSFX") == 1;
            uiMuteSFX.SetActive(isMuted);
            AudioManager.Instance.sfxVol = isMuted ? 0 : 1;
            AudioManager.Instance.initSound(); // Apply the loaded sound settings
        }
    }

    public void MuteSFXON()
    {
        
       
        uiMuteSFX.gameObject.SetActive(!uiMuteSFX.activeSelf);
        AudioManager.Instance.ToggleSFX(); // g?i ToggleSFX() khi k?ch ho?t ho?c t?t uiMuteSFX
    }


    public void MuteAudioON()
    {
      
       
        uiMuteAudio.gameObject.SetActive(!uiMuteAudio.activeSelf);
        AudioManager.Instance.ToggleMusic();
    }


}
