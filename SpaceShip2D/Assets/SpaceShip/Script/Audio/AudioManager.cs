using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pattern;

public class AudioManager : Singleton<AudioManager>
{
    [Header("------ Audio Source ------")]
    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioSource SFXSource;

    public float sfxVol = 1;
    public float SndVol = 1;

    [Header("------ Audio Clip ------")]
    public AudioClip background;
    public AudioClip Shooting;
    public AudioClip Boom;
  



    protected override void Awake()
    {
        if (instance == null)
        {

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            instance = this;
            Destroy(gameObject);
        }
    }


    private void Start()
    {
        initSound();
        LoadSFXSettings();
        LoadAudioSettings();
        if (PlayerPrefs.GetInt("MuteMusic", 0) == 0)
        {
            musicBackgroundOn();
        }

    }

    public void musicBackgroundOn()
    {
        musicSource.clip = background;
        musicSource.Play();
    }

    public void musicBackgroundOff()
    {
        musicSource.clip = background;
        musicSource.Pause();
    }

    public void PlaySFX(AudioClip clip)
    {
        SFXSource.PlayOneShot(clip);
    }


    public void initSound()
    {
        SFXSource.volume = sfxVol;
    }

    public void ToggleSFX()
    {
        if (MenuUI.instance.uiMuteSFX.activeSelf)
        {
            sfxVol = 0; // Muting the sound effects
            PlayerPrefs.SetInt("MuteSFX", 1); // Save state as muted
        }
        else
        {
            sfxVol = 1; // Unmuting the sound effects
            PlayerPrefs.SetInt("MuteSFX", 0); // Save state as unmuted
        }
        PlayerPrefs.Save(); // Make sure to save the PlayerPrefs changes
        initSound(); // Update the volume
    }

    public void LoadSFXSettings()
    {
        if (PlayerPrefs.HasKey("MuteSFX"))
        {
            // Load the SFX volume state
            int muteState = PlayerPrefs.GetInt("MuteSFX");
            sfxVol = (muteState == 1) ? 0 : 1;
            instance.SFXSource.volume = sfxVol;
            MenuUI.instance.uiMuteSFX.SetActive(muteState == 1);
        }
    }

    public void ToggleMusic()
    {
        if (PlayerPrefs.GetInt("MuteMusic", 0) == 1)
        {
            musicBackgroundOn(); // B?t nh?c n?n
            PlayerPrefs.SetInt("MuteMusic", 0); // L?u tr?ng th?i b?t
        }
        else
        {
            musicBackgroundOff(); // T?t nh?c n?n
            PlayerPrefs.SetInt("MuteMusic", 1); // L?u tr?ng th?i t?t
        }
        PlayerPrefs.Save(); // ??m b?o l?u c?c thay ??i c?a PlayerPrefs
    }

    public void LoadAudioSettings()
    {
        if (PlayerPrefs.HasKey("MuteMusic"))
        {
            if (PlayerPrefs.GetInt("MuteMusic") == 1)
            {
                musicBackgroundOff();
            }
            else
            {
                musicBackgroundOn();
            }
        }
    }
}
