using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextShipHP : BaseText
{
    [SerializeField] private Image hpFillImage;

    protected virtual void FixedUpdate()
    {
        this.UpdateShipHP();
    }

    protected virtual void UpdateShipHP()
    {
        int hpMx = PlayerCtrl.Instance.CurrentShip.DamageReceiver.HPMax;
        int hp = PlayerCtrl.Instance.CurrentShip.DamageReceiver.HP;

        this.text.SetText(hp + " / " + hpMx);
        float fillAmount = (float)hp / hpMx;
        hpFillImage.fillAmount = fillAmount;
    }
}
