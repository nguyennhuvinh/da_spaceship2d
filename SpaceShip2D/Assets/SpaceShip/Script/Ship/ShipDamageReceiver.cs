using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDamageReceiver : DamageReceiver
{
    public GameObject parentObject;

    [Header("Shootable Object")]
    [SerializeField] protected ShootableObjectCtrl shootablObjectCtrl;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadCtrl();
    }

    protected virtual void LoadCtrl()
    {
        if (this.shootablObjectCtrl != null) return;
        this.shootablObjectCtrl = transform.parent.GetComponent<ShootableObjectCtrl>();
        Debug.Log(transform.name + ": LoadCtrl", gameObject);
    }


    protected override void OnDead()
    {
       
        this.OnDeadFX();
        

    }

    protected virtual void OnDeadFX()
    {
        
        string fxName = this.GetOnDeadFXName();
        Transform fxOnDead = FXSpawner.Instance.Spawn(fxName, transform.position, transform.rotation);
        AudioManager.Instance.PlaySFX(AudioManager.Instance.Boom);
        fxOnDead.gameObject.SetActive(true);
        StartCoroutine(GameUI.Instance.LoseUI_ON());
        parentObject.SetActive(false);
    }

    protected virtual string GetOnDeadFXName()
    {
        return FXSpawner.explode1;
    }

    public override void Reborn()
    {
        this.hpMax = this.shootablObjectCtrl.ShootableObject.hpMax;
        base.Reborn();
    }

}
