using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Junk", menuName = "ScriptableObjects/Junk")]
public class JunkSO : ScriptableObject
{
    public string junkName = "Junk";
    public int hpMax;
    public List<DropRate> dropList;
}